<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    //
    public function login(Request $request)
    {

        $response = Http::post('http://localhost:8080/api/auth/login', [
            'email' => $request->email,
            'password' => $request->password
        ]);

        $data = json_decode($response);

        if ($data->status ==  false) {
            return response()->json([
                'status' => $data->status,
                'message' => $data->message
            ]);
        }

        AccessToken::create([
            'token' => $data->access_token,
            'data' => json_encode($data->data)
        ]);

        $status = true;

        $result = [
            'status' => $status,
            'message' => 'Successfully logged in',
            'data' => $data,
        ];

        return response()->json($result);
    }
}
